FROM python:3
MAINTAINER Seymur Aliyev
Label version="1.0.1"
RUN python3 -m pip install aiogram
WORKDIR /code
COPY . /code/
ENV Test=1
ENTRYPOINT ["python3", "/code/bot.py" ]
