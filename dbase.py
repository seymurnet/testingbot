import sqlite3

class DBase:

#подключакюсь к бд

    def __init__(self, database_file):
        self.connection = sqlite3.connect(database_file)
        self.cursor = self.connection.cursor()

#пролучаю зарегестрированных пользователей

    def get_registered(self, status=True):
        with self_connection:
            return self.cursor.execute('SELECT * FROM "reggistratedusers" WHERE "status" = ?', (status,)).fetchall()


#есть ли юзер в бд
    def reguser_exist(self, user_id):
        with self.connection:
            result = self.cursor.execute('SELECT * FROM "reggistratedusers" WHERE "user_id" = ?', (user_id,)).fetchall()
            return bool(len(result))


#регистрация юзера
    def add_reguser (self, user_id, status = True):
        with self.connection:
            return self.cursor.execute('INSERT INTO "reggistratedusers" ("user_id", "status") VALUES (?,?)',(user_id, status))



#обновляем статус юзера
    def update_reguser(self, user_id, status):
        return self.cursor.execute('UPDATE "reggistratedusers" SET "status" = ? WHERE "user_id" = ?', (status, user_id))



#закрываем соединение с бд
    def close(self):
        self.connection.close()